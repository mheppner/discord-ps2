from hashlib import sha1

from aiocache import caches

from . import settings


# default config
CONFIG = {
    'default': {
        'cache': 'discord_ps2.cache.EncodedKeyBackend',
        'serializer': {
            'class': 'aiocache.serializers.PickleSerializer'
        },
        'plugins': [
            {'class': 'aiocache.plugins.TimingPlugin'}
        ]
    }
}


# pick a default backend to inherit from
if settings.CACHE_BACKEND == 'memcached':
    from aiocache import MemcachedCache
    backend = MemcachedCache
elif settings.CACHE_BACKEND == 'redis':
    from aiocache import RedisCache
    backend = RedisCache
else:
    from aiocache import SimpleMemoryCache
    backend = SimpleMemoryCache


class EncodedKeyBackend(backend):
    """Backend to encode keys with SHA1.

    This is used to prevent long keys from being used, mostly due to
    Memcached's max key limit of 250.
    """

    def _build_key(self, *args, **kwargs):
        key = super(EncodedKeyBackend, self)._build_key(*args, **kwargs)
        return sha1(str(key).encode()).hexdigest().encode()


# override any user-defined arguments
if settings.CACHE_CONFIG:
    tmp_config = CONFIG['default'].copy()
    tmp_config.update(settings.CACHE_CONFIG)
    CONFIG['default'] = tmp_config

# set the global config
caches.set_config(CONFIG)
