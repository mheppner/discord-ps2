from datetime import datetime, timedelta
import logging

import asyncio
from crontab import CronTab
import discord
import pytz

from discord_ps2 import settings
from discord_ps2.client import client


logger = logging.getLogger(__name__)

# how often the ops should be checked, seconds
CHECK_FREQUENCY = 60


async def ops_alert():
    """Sends an alert to the general channel 1 hour before an ops starts."""
    # wait until the client logs in
    await client.wait_until_ready()

    # go ahead and leave if there are no ops scheduled
    if not len(settings.OUTFIT_OPS):
        logger.info('no outfit ops to watch')
        return

    # figure out what channels to post in
    channels = []
    for channel_id in settings.OUTFIT_OPS_ALERTS_CHANNELS:
        channel = client.get_channel(channel_id)

        if channel is None:
            logger.error('{} is not a valid channel'.format(channel_id))
        elif channel.type.name != 'text':
            logger.error('{} is not a text channel'.format(channel_id))
        else:
            # TODO check that the client has permissions for this channel
            channels.append(channel)

    if not len(channels):
        logger.warn('no valid channels to post outfit ops alerts')
        return

    while not client.is_closed:
        logger.debug('checking for ops')

        # get the current time
        now = pytz.timezone('UTC').localize(datetime.utcnow())

        for cron in settings.OUTFIT_OPS:
            # parse the crontab
            entry = CronTab(cron)

            # get the next start time in seconds
            start_sec = int(entry.next(now))

            # offset the start seconds so we can find when its below the check frequency
            threshold = start_sec - settings.OUTFIT_OPS_ALERTS_BEFORE

            # if the check frequency is between the threshold, its safe to print
            # this ensures we only print the alert once, at the exact time
            if threshold > 0 and threshold <= CHECK_FREQUENCY:
                logger.info('sounding an alarm for an upcoming ops')

                # convert the seconds to the next datetime
                start = now + timedelta(seconds=start_sec)
                tz = pytz.timezone(settings.TIMEZONE).normalize(start)

                # create an output message
                start_str = tz.strftime('%a %b %d at %-I:%M %p %Z')
                output = '⚠️ An upcoming outfit ops is scheduled for {} ⚠️'.format(start_str)

                # send the alert to all channels the client can access
                for channel in channels:
                    await client.send(channel, output)

        # sleep this task and let it loop around for the next check
        await asyncio.sleep(CHECK_FREQUENCY)


client.loop.create_task(ops_alert())
