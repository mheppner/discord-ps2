import asyncio
import logging

from discord.ext import commands

from discord_ps2.bot import bot
from discord_ps2.api import PS2CensusClient, CensusException
from discord_ps2 import settings


class Weapons(commands.Cog):
    """Commands about weapons."""

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.census = PS2CensusClient()

    @commands.command(aliases=['hw'])
    async def hasweapon(self, ctx, weapon_name: str = '', *, player_name: str = '',):
        """check if a player has a weapon"""
        self.logger.info(f'[hasweapon] {player_name} {weapon_name}')

        if not len(player_name):
            await ctx.send('I need a character name to search for')
            return
        if not len(weapon_name):
            await ctx.send('I need a weapon name to search for')
            return

        # get the weapon first
        try:
            weapon = await self.census.item(weapon_name)
        except CensusException as e:
            await ctx.send(str(e))
            return

        # get all of the players to resolve their character ids
        names = player_name.split(' ')
        try:
            characters = await self.census.characters(names)
        except CensusException as e:
            await ctx.send(str(e))
            return

        # get all of the player's weapons
        ids = [x['character_id'] for x in characters]
        try:
            characters_weapons = await self.census.characters_items(ids)
        except CensusException as e:
            await ctx.send(str(e))
            return

        output = []
        for player in characters:
            # check if each character has the weapon
            # TODO flip this loop
            for item in characters_weapons:
                if item['character_id'] == player['character_id'] and \
                        item['item_id'] == weapon['item_id']:
                    output.append(f'✅ {player["name"]["first"]} owns the {weapon["name"]["en"]}')
                    break
            else:
                output.append(
                    f'❌ {player["name"]["first"]} does not own the {weapon["name"]["en"]}')

        await ctx.send('\n' + '\n'.join(output))

    @commands.command(aliases=['ow'])
    async def outfitweapon(self, ctx, weapon_name: str = '', outfit_name: str = ''):
        """check if online outfit members have a weapon"""
        if not len(weapon_name):
            await ctx.send('I need a weapon name to search for')
            return

        # get the weapon first
        try:
            weapon = await self.census.item(weapon_name)
        except CensusException as e:
            await ctx.send(str(e))
            return

        # get the online members for the outfit
        try:
            _, members = await self.census.online_outfit_members(
                alias=outfit_name.strip())
        except CensusException as e:
            await ctx.send(str(e))
            return

        # get all of the players' weapons
        ids = [x['character_id'] for x in members]
        try:
            characters_weapons = await self.census.characters_items(ids)
        except CensusException as e:
            await ctx.send(str(e))
            return

        output = []
        for player in members:
            # check if each character has the weapon
            # TODO flip this loop
            for item in characters_weapons:
                if item['character_id'] == player['character_id'] and \
                        item['item_id'] == weapon['item_id']:
                    output.append((
                        f'✅ {player["character"]["name"]["first"]} owns the '
                        f'{weapon["name"]["en"]}'))
                    break
            else:
                output.append((
                    f'❌ {player["character"]["name"]["first"]} does not own the '
                    f'{weapon["name"]["en"]}'))

        await ctx.send('\n' + '\n'.join(output))


bot.add_cog(Weapons(bot))
