"""Module for registering all commands.

To keep the bot commands organized, place each one in a new file in this
directory. Import the function (or everything) from each file below to
register them with the bot.
"""
from .extra import *
from .outfit import *
from .server import *
from .statistics import *
from .weapons import *
