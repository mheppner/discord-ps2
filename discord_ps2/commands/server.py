import logging

from discord.ext import commands

from discord_ps2 import settings
from discord_ps2.api import helper
from discord_ps2.bot import bot
from discord_ps2.api import (PS2CensusClient, CensusException, FISUClient,
                             FISUException, WARPGATES, ALERTS)
from discord_ps2.utils import find_emoji, find_faction_emoji, convert_timestamp


class Server(commands.Cog):
    """All server- and continent-related commands."""

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.census = PS2CensusClient()
        self.fisu = FISUClient()

    @commands.command(aliases=['ss'])
    async def serverstatus(self, ctx):
        """check server status

        Lists available worlds (servers) and whether or not they are online.
        """
        self.logger.info('[serverstatus]')

        try:
            worlds = await self.census.worlds()
        except CensusException as e:
            await ctx.send(str(e))
            return

        output = 'Server status:\n'
        for server in worlds:
            output += f'\t{server["name"]["en"]} - {server["state"]}\n'
        await ctx.send(output)

    @commands.command(aliases=['sp'])
    async def serverpop(self, ctx, world_name: str = ''):
        """see population estimate for a server"""
        self.logger.info(f'[serverpop] {world_name}')

        try:
            world = await self.census.world(name=world_name.strip())
        except CensusException as e:
            await ctx.send(str(e))
            return

        try:
            pop = await self.fisu.world_population(world['world_id'])
        except FISUException as e:
            await ctx.send(str(e))
            return

        # calculate the population percentage
        total = pop['vs'] + pop['nc'] + pop['tr']
        vs = int(round(float(pop['vs']) / float(total) * 100))
        nc = int(round(float(pop['nc']) / float(total) * 100))
        tr = int(round(float(pop['tr']) / float(total) * 100))

        # try getting a custom emoji for the factions
        vs_emoji = find_emoji(self.bot, 'vanu') or '**VS**'
        nc_emoji = find_emoji(self.bot, 'nc') or '**NC**'
        tr_emoji = find_emoji(self.bot, 'tr') or '**TR**'

        # if an emoji was found, use it, otherwise just use a text label
        output = f'{world["name"]["en"]} server population:\n'
        output += f'\t{vs_emoji} {vs}% ({pop["vs"]})\n'
        output += f'\t{nc_emoji} {nc}% ({pop["nc"]})\n'
        output += f'\t{tr_emoji} {tr}% ({pop["tr"]})\n'
        await ctx.send(output)

    @commands.command(aliases=['c'])
    async def continents(self, ctx, name: str = ''):
        """see continent status for a world"""
        self.logger.info(f'[continents] {name}')

        try:
            # lookup by name, if provided, or default to the set world id
            if name:
                world = await self.census.world(name=name)
            else:
                world = await self.census.world()
        except CensusException as e:
            await ctx.send(str(e))
            return

        try:
            # get map data and recent world metagame events
            data_map = await self.census.map(world['world_id'])
            data_events = await self.census.world_events(world['world_id'])
        except CensusException as e:
            await ctx.send(str(e))
            return

        # find alert status
        alert = None
        if len(data_events):
            # the first alert event should (theoretically) be the most recent alert event
            # if the multiple-alert bug doesn't come back, we only need this single event
            alert_event = data_events[0]

            # only look at started events
            if alert_event['metagame_event_state_name'] == 'started':
                # there isn't an easy way to map an alert to the actual region
                try:
                    zone_id = ALERTS[alert_event['metagame_event_id']]
                except KeyError:
                    self.logger.warning(
                        f'unknown metagame event: {alert_event["metagame_event_id"]}')
                else:
                    # only save the started event, with the found continent and converted timestamp
                    alert = alert_event
                    alert['continent'] = helper.zones[zone_id]
                    alert['start'] = convert_timestamp(alert_event['timestamp'])

        output = f'Continent status for {world["name"]["en"]}:\n'

        # loop through each continent and check
        for continent in data_map:
            # lookup the details of the zone
            zone = helper.zones[continent['ZoneId']]
            # keep track of the warpgates for this continent
            warpgates = []

            # loop through every region/facility on the continent
            for region in continent['Regions']['Row']:
                # check if this region is a known warpgate for this continent
                if region['RowData']['RegionId'] in WARPGATES[continent['ZoneId']]:
                    # get the action zone object
                    warpgate = helper.find_base_by_region(continent['ZoneId'],
                                                          region['RowData']['RegionId'])
                    # get the faction object
                    faction = helper.factions[region['RowData']['FactionId']]
                    # keep track of the warp gates
                    warpgates.append({
                        'region': warpgate,
                        'faction': faction
                    })

            # get list of factions owning the warpgates
            owners = [x['faction']['faction_id'] for x in warpgates]
            # the continent is locked if all the warpgate factions are the same
            is_locked = owners.count(owners[0]) == len(owners)

            if is_locked:
                output += f'**{zone["name"]["en"]}** 🔒\n'
                output += f'\tLocked by the {warpgates[0]["faction"]["name"]["en"]}\n'

                # move on to next continent
                continue

            output += f'**{zone["name"]["en"]}**'

            # if the ongoing alert matches the continent, add text
            if alert and alert['continent']['zone_id'] == zone['zone_id']:
                output += ' ⚠️\n'
                output += '\tALERT started on {}'.format(
                    alert['start'].strftime('%a %b %d at %I:%M %p %Z'))

            output += '\n'

            # print the faction owners of the warpgates
            for warpgate in warpgates:
                emoji = find_faction_emoji(self.bot, warpgate['faction']['faction_id'])
                output += f'\t{emoji} owns the {warpgate["region"]["facility_name"]}\n'

        await ctx.send(output)


bot.add_cog(Server(bot))
