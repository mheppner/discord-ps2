import asyncio
import logging
import math

from aiohttp import ClientSession, ClientError
from aiocache import cached

from discord_ps2 import settings
from .constants import CONTINENT_ZONES


MAX_LIMIT = 1000  # max objects per page
session = ClientSession(raise_for_status=True, conn_timeout=30)


class CensusException(Exception):
    """Any PS2 client error."""

    pass


class PS2CensusClient(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    async def _fetch(self, url: str, params: dict = None):
        """Fetch a URL with params using the internal session.

        :param url: the url to GET
        :param params: any GET params to attach to the request
        """
        try:
            async with session.get(url, params=params) as res:
                data = await res.json()
        except ClientError as e:
            self.logger.error('failed to fetch PS2 API', e)
            raise CensusException(e)
        else:
            if 'error' in data:
                raise CensusException(data['error'])
            if 'errorCode' in data:
                raise CensusException(data['errorCode'])
            return data

    async def _paginate(self, url: str, total: int, params: dict = None,
                        per_page: int = MAX_LIMIT):
        """Paginate a data set by making multiple requests in parallel.

        By using c:limit and c:start, a full result set can be paginated.
        Each request for the pages can be ran on the event loop in parallel,
        and the data will be returned onces all pages complete.

        :param url: the url to GET
        :param total: the total number of expected objects
        :param params: any GET params to attach to each request
        :param per_page: number of items per page
        """
        # use an empty dict, or copy the one passed in
        params = {} if params is None else params.copy()

        # set default params for page limit
        params['c:limit'] = per_page
        params['c:limitPerDB'] = per_page

        # num of pages is upper bound of total known items / items per page
        num_pages = math.ceil(total / per_page)

        # create params for querying each page
        tasks = []
        for page in range(num_pages):
            # start at the current bound
            offset = page * per_page

            # copy the base params and add the offset
            page_params = params.copy()
            page_params['c:start'] = offset

            task = asyncio.ensure_future(self._fetch(url, params=page_params))
            tasks.append(task)

        # get all pages at once
        return await asyncio.gather(*tasks)

    @cached(alias='default', ttl=60 * 60 * 3)
    async def outfit(self, alias: str = '',
                     id: str = settings.OUTFIT_ID,
                     params: dict = None):
        """Get an outfit.

        Looking up the outfit by alias will cause a slower query. When
        possible, only lookup by ID.

        :param alias: an outfit alias to lookup by
        :param id: the id to use
        :param params: overridden query params
        """
        query = {
            'c:limit': 1
        }

        if alias:
            query['alias'] = alias
            query['c:case'] = 'false'
        else:
            query['outfit_id'] = id
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'outfit'
        data = await self._fetch(url, query)

        if len(data['outfit_list']):
            return data['outfit_list'][0]
        raise CensusException('That outfit does not exist')

    @cached(alias='default', ttl=60 * 2)
    async def outfit_members(self, alias: str = '',
                             id: str = settings.OUTFIT_ID,
                             params: dict = None):
        """List outfit members.

        :param alias: lookup the outfit by alias
        :param id: the id to use
        :param params: overridden query params
        """
        outfit = await self.outfit(alias=alias, id=id)
        member_count = int(outfit['member_count'])

        query = {
            'outfit_id': outfit['outfit_id'],
            'c:resolve': 'online_status,character_name',
            'c:show': 'character_id,rank,rank_ordinal',
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'outfit_member'

        # get all pages
        data = await self._paginate(url, params=query,
                                    total=member_count)

        # rejoin into a single list
        members = []
        for page in data:
            members += page['outfit_member_list']
        return members

    async def online_outfit_members(self, alias: str = '',
                                    id: str = settings.OUTFIT_ID,
                                    params: dict = None):
        """List only online outfit members.

        :param alias: lookup the outfit by alias
        :param id: the id to use
        :param params: overridden query params
        :returns: total count, online members list
        """
        members = await self.outfit_members(alias=alias, id=id, params=params)
        total = len(members)
        online_members = [x for x in members
                          if x['online_status'].isdigit() and int(x['online_status']) > 0]
        return total, online_members

    @cached(alias='default', ttl=60 * 15)
    async def characters(self, names: list, params: dict = None):
        """Get a character.

        :param names: the character names
        :param params: overridden query params
        """
        names = [x.lower().strip() for x in names]
        query = {
            'name.first_lower': ','.join(names),
            'c:limit': len(names),
            'c:resolve': 'outfit',
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'character'

        data = await self._fetch(url, query)
        if len(data['character_list']):
            return data['character_list']
        raise CensusException('Those characters do not exist')

    @cached(alias='default', ttl=60 * 2)
    async def character_events(self, id: str,
                               event_types: str = 'FACILITY_CHARACTER',
                               limit: int = MAX_LIMIT, params: dict = None):
        query = {
            'character_id': id,
            'type': event_types,
            'c:limit': limit
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'characters_event'

        data = await self._fetch(url, query)
        if len(data['characters_event_list']):
            return data['characters_event_list']
        raise CensusException('No recent events for that player exist')

    @cached(alias='default', ttl=60 * 2)
    async def characters_events(self, ids: list, event_type: str,
                                params: dict = None):
        """Get events for multiple characters.

        This will filter by all of the character IDs passed in and the event
        type. Since there is a length limit to URLs, the character IDs are
        chunked to fit the limit, and multiple pages are requested in
        parallel. This isn't exactly accurate, but if the MAX_LIMIT is set
        high enough, it mostly won't be an issue.

        :param ids: the list of character IDs to filter by
        :param event_type: the type of events to filter by
        :param params: overridden query params
        """
        query = {
            'type': event_type,
            'c:limit': MAX_LIMIT,
            'c:lang': 'en',
            'c:resolve': 'character_name'
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'characters_event'

        # force a limit on number of ids to query per page
        # (url length limit - base for url) / length of character ids
        ids_limit = int((2038 - 150) / 19)

        # split the ids into buckets of pages
        pages = [ids[i:i + ids_limit] for i in range(0, len(ids), ids_limit)]

        tasks = []
        for paged_ids in pages:
            # copy the base params and add the offset
            page_params = query.copy()
            page_params['character_id'] = ','.join(paged_ids)

            task = asyncio.ensure_future(self._fetch(url, params=page_params))
            tasks.append(task)

        # get all pages at once
        data = await asyncio.gather(*tasks)

        # merge together and sort descending by timestamp
        all_events = []
        for event in data:
            all_events += event['characters_event_list']
        all_events.sort(key=lambda x: int(x['timestamp']), reverse=True)
        return all_events

    @cached(alias='default', ttl=60 * 30)
    async def characters_items(self, ids: list, params: dict = None):
        """"""
        query = {
            'character_id': ','.join(ids)
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'characters_item'

        # force a limit on number of ids to query per page
        # (url length limit - base for url) / length of character ids
        ids_limit = int((2038 - 150) / 19)

        # split the ids into buckets of pages
        pages = [ids[i:i + ids_limit] for i in range(0, len(ids), ids_limit)]

        tasks = []
        for paged_ids in pages:
            # copy the base params and add the offset
            page_params = query.copy()
            page_params['character_id'] = ','.join(paged_ids)

            task = asyncio.ensure_future(self._fetch(url, params=page_params))
            tasks.append(task)

        # get all pages at once
        data = await asyncio.gather(*tasks)

        # merge together
        all_items = []
        for item in data:
            all_items += item['characters_item_list']
        return all_items

    @cached(alias='default', ttl=60 * 60 * 6)
    async def worlds(self, params: dict = None):
        """Get world (continent) information.

        :param params: overridden query params
        """
        query = {
            'c:limit': 25,
            'c:lang': 'en'
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'world'

        data = await self._fetch(url, query)
        return data['world_list']

    async def world(self, name: str = '', id: str = settings.WORLD_ID):
        """Get a single world (continent) information.

        Since querying by name is not supported, all worlds() are fetched
        first and manually filtered.

        :param name: find by the name
        :param id: find by the id
        """
        worlds = await self.worlds()
        for world in worlds:
            if name:
                if world['name']['en'].lower() == name.lower():
                    return world
            else:
                if world['world_id'] == str(id):
                    return world
        raise CensusException('That world does not exist')

    @cached(alias='default', ttl=60 * 60 * 6)
    async def factions(self, params: dict = None):
        """Get faction information.

        :param params: overridden query params
        """
        query = {
            'c:limit': 10,
            'c:lang': 'en'
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'faction'

        data = await self._fetch(url, query)
        return data['faction_list']

    @cached(alias='default', ttl=60 * 60 * 12)
    async def map(self, world_id: str, params: dict = None):
        """Fetch map data for a particular world.

        :param world_id: the id of the world to query
        :param params: overridden query params
        """
        query = {
            'world_id': world_id,
            'zone_ids': ','.join(CONTINENT_ZONES)
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'map'

        data = await self._fetch(url, query)
        if len(data['map_list']):
            return data['map_list']
        raise CensusException('Map data could not be found')

    @cached(alias='default', ttl=60 * 1)
    async def world_events(self, id: str, type: str = 'METAGAME',
                           params: dict = None):
        query = {
            'type': 'METAGAME',
            'world_id': id,
            'c:limit': 1,
            'c:lang': 'en',
            'c:join': 'type:metagame_event^on:metagame_event_id^inject_at:details^list:0'
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'world_event'

        data = await self._fetch(url, query)
        if len(data['world_event_list']):
            return data['world_event_list']
        raise CensusException('No world events could be found')

    @cached(alias='default', ttl=60 * 60 * 12)
    async def item(self, name: str, params: dict = None):
        query = {
            'name.en': '*' + name,  # name contains
            'c:case': 'false',  # case insensitive search
            'c:limit': '1',
            'c:lang': 'en',
            'c:show': 'item_id,name',
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'item'

        data = await self._fetch(url, query)
        if len(data['item_list']):
            return data['item_list'][0]
        raise CensusException('That item does not exist')

    @cached(alias='default', ttl=60 * 60 * 12)
    async def zones(self, params: dict = None):
        """Load continents/zones with their bases/regions."""
        query = {
            'c:join': ('type:map_region^list:1^inject_at:regions'
                       "^hide:zone_id'location_x'location_y'location_z"),
            'c:limit': '15',
            'c:lang': 'en'
        }
        if params is not None:
            query.update(params)
        url = settings.PS2_BASE_URL + 'zone'

        data = await self._fetch(url, query)
        if len(data['zone_list']):
            return data['zone_list']
        raise CensusException('No zones could be found')
