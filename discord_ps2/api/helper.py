from functools import lru_cache

from discord_ps2 import settings
from .census import PS2CensusClient


class PlanetsideHelper(object):

    def __init__(self):
        self.census = PS2CensusClient()
        self._has_defaults = False
        self.zones = {}
        self.worlds = {}
        self.factions = {}

    async def load_defaults(self):
        """Load static items to allow for quicker lookups later on."""
        # load zones and remap
        zones = await self.census.zones()
        self.zones = {x['zone_id']: x for x in zones}

        # load worlds and remap
        worlds = await self.census.worlds()
        self.worlds = {x['world_id']: x for x in worlds}

        # load factions and remap
        factions = await self.census.factions()
        self.factions = {x['faction_id']: x for x in factions}

        self._has_defaults = True

    @lru_cache(maxsize=1024)
    def find_base_by_facility(self, zone_id, facility_id):
        """Lookup a base/facility based on the continent id and the facility id.

        This assumes that the zones have already been loaded and zone_id exists in the data. In
        order to prevent multiple unnecessary loops, a LRU cache is used.
        """
        for region in self.zones[zone_id]['regions']:
            if region['facility_id'] == facility_id:
                return region

    @lru_cache(maxsize=1024)
    def find_base_by_region(self, zone_id, region_id):
        """Lookup a base/facility based on the continent id and the region id."""
        for region in self.zones[zone_id]['regions']:
            if region['map_region_id'] == region_id:
                return region


helper = PlanetsideHelper()
