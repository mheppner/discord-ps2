"""Source module.

No changes will need to be made to this file.
"""
# use uvloop if available
import asyncio
try:
    import uvloop
except ImportError:
    pass
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

# ensure cache config gets loaded first
from . import cache

from .commands import *
from .tasks import *
