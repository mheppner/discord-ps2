"""Load all config options through environment variables.

This allows for easily changing any setting before runtime. All environment
variables are prefixed with DISCORDPS2_ and should be stored in a ".env" file
in the project root.
"""
from os.path import join, dirname

from environs import Env
from marshmallow.validate import OneOf

PREFIX = 'DPS2_'


# read env file from ../.env
env_path = join(dirname(dirname(__file__)), '.env')
env = Env()
env.read_env(env_path)

# load prefixed env vars
with env.prefixed(PREFIX):
    BOT_PREFIX = env.str('BOT_PREFIX', default='!')

    # description text for the bot
    BOT_DESCRIPTION = env.str('BOT_DESCRIPTION',
                              default='Planetside 2 Discord bot.')

    # login token for Discord's API, required
    DISCORD_AUTH_TOKEN = env.str('DISCORD_AUTH_TOKEN')

    # the token for accessing the DBG API, required
    PS2_SERVICE_ID = env.str('PS2_SERVICE_ID')

    # type of cache to use
    CACHE_BACKEND = env.str('CACHE_BACKEND', default='memory', validate=OneOf(
        ['memory', 'redis', 'memcached'],
        error='CACHE_BACKEND must be one of : {choices}'))

    # any overridden arguments for the cache region
    CACHE_CONFIG = env.dict('CACHE_CONFIG', default='')

    # the root url of the API
    PS2_BASE_URL = env.str(
        'PS2_BASE_URL',
        default='https://census.daybreakgames.com/s:{}/get/ps2:v2/'.format(
            PS2_SERVICE_ID))

    # root url of the fisu API
    FISU_BASE_URL = env.str('FISU_BASE_URL', default='http://ps2.fisu.pw/api/')

    # outfit id number, required
    OUTFIT_ID = env.str('OUTFIT_ID')

    # default server id, defaults to emerald
    WORLD_ID = env.int('WORLD_ID', default=17)

    # set the timezone to display items in
    TIMEZONE = env.str('TIMEZONE', default='America/New_York')

    # when ops happen
    # use a list of crontabs in UTC times
    # 0 20 * * 6 is every friday at 8pm
    OUTFIT_OPS = env.list('OUTFIT_OPS', default=[])

    # number of seconds before an ops the alert should be shown
    OUTFIT_OPS_ALERTS_BEFORE = env.int('OUTFIT_OPS_ALERTS_CHANNELS',
                                       default=60 * 60)

    # channels to post ops alerts into
    OUTFIT_OPS_ALERTS_CHANNELS = env.list('OUTFIT_OPS_ALERTS_CHANNELS',
                                          default=[])

    # the "now playing" status text for the bot
    NOW_PLAYING = env.str('NOW_PLAYING', default='Planetside 2')

    # a dict of users ids with emojis to react to
    # example: 12345=🥝,6789=🍆,7777=customemojiname
    RANDOM_REACTION_USERS = env.dict('RANDOM_REACTION_USERS', default='')

    # the chance of the bot reacting to one of the tracked users
    RANDOM_REACTION_CHANCE = env.float('RANDOM_REACTION_CHANCE', default=0.05)

    # positive and negative responses for the !iscool command
    ISCOOL_POSITIVE_RESPONSES = env.list(
        'ISCOOL_POSITIVE_RESPONSES',
        default=['{} is pretty neat',
                 '{} is dank af',
                 '{}, a god amongst men',
                 'give {} a 🏅'])
    ISCOOL_NEGATIVE_RESPONSES = env.list(
        'ISCOOL_NEGATIVE_RESPONSES',
        default=['{} can suck a bag of dicks',
                 '{} is a huge nerd',
                 '{} should be perma banned',
                 'this outfit should collectively team kill {}',
                 '{} is 💩'])
