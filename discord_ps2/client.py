"""Main definition for the client object."""
import logging
import random

import discord

from .utils import find_emoji
from . import settings


logger = logging.getLogger(__name__)

client = discord.AutoShardedClient()


@client.event
async def on_ready():
    logger.info('client ready')


@client.event
async def on_message(message):
    if settings.RANDOM_REACTION_USERS is not None:
        # randomly react to selected user's messages with an emoji
        for uid, emoji_name in settings.RANDOM_REACTION_USERS.items():
            # the current message was written by someone to target
            if message.author.id == uid:
                # decide if the reaction should happen
                should_react = random.random() <= settings.RANDOM_REACTION_CHANCE

                if should_react:
                    # try finding the custom emoji first
                    emoji = find_emoji(client, emoji_name)

                    if emoji is None:
                        # assume it's an actual unicode emoji
                        emoji = emoji_name

                    # attach the emoji to the message
                    await client.add_reaction(message, emoji)

                # no need to check the rest of the tracked users
                break
