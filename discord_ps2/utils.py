"""Miscellaneous utilities."""
from datetime import datetime
from functools import lru_cache

import pytz

from . import settings
from .api.constants import FACTIONS_SHORT


@lru_cache(maxsize=1024)
def find_emoji(bot, name):
    """finds a custom emoji in the server by name."""
    for emoji in bot.emojis:
        if emoji.name == name:
            return emoji


@lru_cache(maxsize=1024)
def find_faction_emoji(bot, faction_id):
    """finds a emoji for a faction, defaulting to a short label."""
    emoji = ''
    if faction_id in FACTIONS_SHORT:
        if faction_id == '1':
            emoji = find_emoji(bot, 'vanu')
        elif faction_id == '2':
            emoji = find_emoji(bot, 'nc')
        elif faction_id == '3':
            emoji = find_emoji(bot, 'tr')

        if not emoji:
            emoji = FACTIONS_SHORT[faction_id]
    return emoji


def convert_timestamp(timestamp):
    """converts a UTC timestamp to a local datetime."""
    timestamp = int(timestamp)
    utc_date = pytz.timezone('UTC').localize(
        datetime.utcfromtimestamp(timestamp))
    local_date = pytz.timezone(settings.TIMEZONE).normalize(utc_date)
    return local_date
