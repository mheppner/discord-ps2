# Planetside 2 Discord bot

This is a Discord bot for Planetside 2, designed for an outfit-based Discord server.

Supported commands:

- Outfit:
    - `deaths` - show recent player deaths in the outfit
    - `iscool <player_name>` - check if an outfit member has been online in the past 12 hours
    - `online` - see all outfit members currently online
    - `ops` - show when the next outfit ops occurs
- Server:
    - `continents <world_id>` - see continent status for a server
    - `serverpop <world_id>` - see population estimate for a server
    - `serverstatus` - see server status of all servers
- Statistics:
    - `findoutfit <outfit_alias>` - find where an outfit is playing at
    - `findplayer <player_name>` - find where a player is fighting
- Extra:
    - `rps <choice>` - play rock paper scissors with the bot
    - `rtd` - rolls the dice
    - `textcolor <color> <message>` - create a colored text string for squad and platoon names

Background tasks:

- `outfit_ops` - posts an alert 1 hour prior to an outfit ops to a specific channel


## Requirements and installation

Install Python 3.5 or 3.6 and [`pip`](https://pip.pypa.io/en/stable/). Once installed, set up the environment for this project:

```sh
# clone the repo
git pull https://gitlab.com/mheppner/discord-ps2.git
cd discord-ps2

# create a local virtual environment with your version of Python
python3.6 -m venv env

# activate your shell with the virtual environment
source env/bin/activate

# upgrade pip and install requirements
pip install -U pip
pip install -r requirements.txt

# create a settings file
cp .env.sample .env
```

After working on the project, you can leave the context of your virtualenv by using the `deactivate` command.

To start the bot without entering the virtualenv, use the full path to the Python executable:

```sh
<base_path>/env/bin/python <base_path>main.py
```

### Configuration

Set environment variables in the `.env` file, each one prefixed with `DPS2_`. See the `.env.sample` file for required parameters.

- `DPS2_BOT_PREFIX` *(default: '!')* - the prefix to invoke commands from within Discord
- `DPS2_BOT_DESCRIPTION` *(default: 'Planetside 2 Discord bot.')* - the description to show in the help menu
- `DPS2_DISCORD_AUTH_TOKEN` **required** - the auth token for connecting the bot to your Discord server
- `DPS2_PS2_SERVICE_ID` **required** - the service token for calling the Daybreak API
- `DPS2_CACHE_BACKEND` *(default: 'memory')* - the backend to use for caching data from API calls (either memory, redis, or memcached)
- `DPS2_CACHE_CONFIG` *(default: '')* - additional options for the selected cache backend
- `DPS2_BASE_URL` *(default: 'https://census.daybreakgames.com/s:<auth-token>/get/ps2:v2')* - the base URL for all Daybreak API calls
- `DPS2_FISU_BASE_URL` *(default: 'http://ps2.fisu.pw/api/')* - the base URL for connecting to the FISU API
- `DPS2_OUTFIT_ID` **required** - your outfit ID, found using the Daybreak API
- `DPS2_WORLD_ID` *(default: 17)* - the world/continent ID your outfit resides on
- `DPS2_TIMEZONE` *(default: 'America/New_York')* - the default timezone for converting all times into
- `DPS2_OUTFIT_OPS` *(default: [])* - a list of crontabs for when outfit ops happen
- `DPS2_OUTFIT_OPS_ALERTS_BEFORE` *(default: 3600)* - the time in seconds of when an alert should be posted prior to an outfit ops
- `DPS2_OUTFIT_OPS_ALERTS_CHANNELS` *(default: [])* - the list of channel IDs that ops alerts should post into
- `DPS2_NOW_PLAYING` *(default: 'Planetside 2')* - the prefix of the text used to display the now playing status of the bot
- `DPS2_RANDOM_REACTION_USERS` *(default: {})* - a dict mapping Discord user IDs to an emoji the bot will randomly add reactions to
- `DPS2_RANDOM_REACTION_CHANCE` *(default: 0.05)* - a percentage of how often the reaction should happen

## Usage

To start the bot, use the `main.py` script. Supply `-h`/`--help` to see available options.

By default, logging will be set to use stdout/stderr on the WARNING level. Use `-v`/`--verbose` for INFO logging and `-d`/`--debug` for DEBUG logging.

### Docker

A pre-built docker image is available at [mheppner/discord-ps2](https://hub.docker.com/r/mheppner/discord-ps2). Run with no arguments, supplying the environment variables as needed:

```sh
docker run \
    -e DPS2_DISCORD_AUTH_TOKEN=your-auth-token \
    -e DPS2_PS2_SERVICE_ID=your-api-id \
    -e DPS2_OUTFIT_ID=your-outfit-id
    mheppner/discord-ps2
```
